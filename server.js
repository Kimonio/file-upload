// File upload script.
var express =   require("express");
var multer  =   require('multer');
var app     =   express();
var storage =   multer.diskStorage({
	destination: function (req, file, callback) {
		callback(null, './uploads');
	},
	filename: function (req, file, callback) {
		callback(null, file.fieldname + '.json');
	}
});
var upload = multer({ storage : storage}).single('userData');

// Handle CORS requests.
app.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization');
	next();
});

// File Upload Page.
app.get('/',function(req,res){
	res.sendFile(__dirname + "/index.html");
});

// API.
app.get('/data',function(req,res){
	res.sendFile(__dirname + "/uploads/userData.json");
});

app.post('/api/userData',function(req,res){
	upload(req, res, function(err) {
		if(err) {
			return res.end("Error uploading file.");
		}
		res.end("File is uploaded");
	});
});

// set the port of our application
// process.env.PORT lets the port be set by Heroku
var port = process.env.PORT || 8080;

app.listen(port, function() {
	console.log('Our app is running on http://localhost:' + port);
});